%{!?delete_la: %global delete_la  find $RPM_BUILD_ROOT -type f -name "*.la" -delete}

Name:          unbound
Version:       1.17.1
Release:       11
Summary:       Unbound is a validating, recursive, caching DNS resolver
License:       BSD-3-Clause
Url:           https://nlnetlabs.nl/projects/unbound/about/
Source:        https://nlnetlabs.nl/downloads/unbound/%{name}-%{version}.tar.gz
Source1:       unbound.service
Source2:       unbound.conf
Source3:       root.key
Source4:       unbound-keygen.service
Source5:       tmpfiles-unbound.conf
Source6:       example.com.key
Source7:       example.com.conf
Source8:       block-example.com.conf
Source9:       https://data.iana.org/root-anchors/icannbundle.pem
Source10:      root.anchor
Source11:      unbound.sysconfig
Source12:      unbound-anchor.timer
Source13:      unbound-anchor.service

Patch1:        unbound-remove-buildin-key.patch
Patch2:        backport-CVE-2023-50387_CVE-2023-50868.patch
Patch3:        backport-pre-CVE-2024-33655-extended_error_encode-for-extended-errors.patch
Patch4:        backport-pre-CVE-2024-33655-Downstream-DNS-Cookies-a-la-RFC7873-and-RFC9018.patch
Patch5:        backport-pre-CVE-2024-33655-Fix-possibly-unaligned-memory-access-in-parse_edns_options_from_query.patch
Patch6:        backport-pre-CVE-2024-33655-Fix-out-of-bounds-read-in-parse_edns_options_from_query.patch
Patch7:        backport-CVE-2024-33655.patch
Patch8:        backport-CVE-2024-43167.patch
Patch9:        backport-001-CVE-2024-43168.patch
Patch10:       backport-002-CVE-2024-43168.patch
Patch11:       backport-003-CVE-2024-43168.patch
Patch12:       backport-004-CVE-2024-43168.patch
Patch13:       backport-CVE-2024-8508.patch
Patch14:       backport-check-before-use-daemon-shm_info.patch

BuildRequires: make flex swig pkgconfig systemd
BuildRequires: libevent-devel expat-devel openssl-devel python3-devel
BuildRequires: gcc byacc

%{?systemd_requires}
Requires:      %{name}-libs = %{version}-%{release}
Requires:      %{name}-anchor = %{version}-%{release}
Recommends:    %{name}-utils = %{version}-%{release}
Requires:      openssl

%description
Unbound is a validating, recursive, caching DNS resolver. It is designed
to be fast and lean and incorporates modern features based on open standards.
To help increase online privacy, Unbound supports DNS-over-TLS which allows
clients to encrypt their communication. Unbound is available for most platforms
such as FreeBSD, OpenBSD, NetBSD, MacOS, Linux and Microsoft Windows.
Unbound is a totally free, open source software under the BSD license. It doesn't
make custom builds or provide specific features to paying customers only.

%package libs
Summary:       Libraries for %{name}
Requires(pre): shadow-utils

%description libs
Libraries for %{name}.

%package anchor
Requires: %{name}-libs = %{version}-%{release}
Summary: DNSSEC trust anchor maintaining tool

%description anchor
Contains tool maintaining trust anchor using RFC 5011 key rollover algorithm.

%package utils
Requires: %{name}-libs = %{version}-%{release}
Summary: Unbound DNS lookup utilities

%description utils
Contains tools for making DNS queries. Can make queries to DNS servers
also over TLS connection or validate DNSSEC signatures. Similar to
bind-utils.

%package devel
Summary:       Libraries and header files
Requires:      %{name}-libs = %{version}-%{release} openssl-devel pkgconfig

%description devel
Package devel includes libraries and header files for development with unbound.

%package -n python3-%{name}
Summary:       The python3 module of unbound DNS resolver
Requires:      %{name}-libs = %{version}-%{release}

%description -n python3-unbound
The python3 module of unbound DNS resolver.

%package help
Summary: Man pages for unbound

%description help
Package help includes includes man pages for unbound.

%prep

%setup -qcn %{name}-%{version}

pushd %{name}-%{version}

%autopatch -p1

cp -pr doc pythonmod libunbound ../
popd

%build
%global configure_args --with-libevent --with-pthreads --with-ssl \\\
            --disable-rpath --disable-static \\\
            --enable-relro-now --enable-pie \\\
            --enable-subnet --enable-ipsecmod \\\
            --with-conf-file=%{_sysconfdir}/%{name}/unbound.conf \\\
            --with-pidfile=%{_rundir}/%{name}/%{name}.pid \\\
            --enable-sha2 --disable-gost --enable-ecdsa \\\
            --with-rootkey-file=%{_sharedstatedir}/unbound/root.key \\\
            --enable-linux-ip-local-port-range

pushd %{name}-%{version}
%configure --with-pythonmodule --with-pyunbound PYTHON=%{__python3} --disable-sha1 %{configure_args}
%make_build
%make_build streamtcp
popd

%install

pushd %{name}-%{version}
%make_install unbound-event-install
install -m 0755 streamtcp $RPM_BUILD_ROOT%{_sbindir}/unbound-streamtcp
install -d -m 0755 $RPM_BUILD_ROOT%{_unitdir} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -m 0644 testcode/streamtcp.1 $RPM_BUILD_ROOT%{_mandir}/man1/unbound-streamtcp.1
install -D -m 0644 contrib/libunbound.pc $RPM_BUILD_ROOT%{_libdir}/pkgconfig/libunbound.pc
popd

install -d -m 0755 $RPM_BUILD_ROOT%{_tmpfilesdir} $RPM_BUILD_ROOT%{_sharedstatedir}/unbound
install -m 0644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/unbound/
install -m 0644 %{SOURCE5} $RPM_BUILD_ROOT%{_tmpfilesdir}/unbound.conf
%if %{?openEuler:1}0
install -m 0644 %{SOURCE10} $RPM_BUILD_ROOT%{_sharedstatedir}/unbound/root.key
%else
install -m 0600 %{SOURCE10} $RPM_BUILD_ROOT%{_sharedstatedir}/unbound/root.key
%endif

install -p -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}/unbound.service
install -p -m 0755 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/unbound
install -p -m 0644 %{SOURCE4} $RPM_BUILD_ROOT%{_unitdir}/unbound-keygen.service
install -p -m 0644 %{SOURCE9} $RPM_BUILD_ROOT%{_sysconfdir}/unbound
install -p -m 0644 %{SOURCE11} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/unbound
install -p -m 0644 %{SOURCE12} $RPM_BUILD_ROOT%{_unitdir}/unbound-anchor.timer
install -p -m 0644 %{SOURCE13} $RPM_BUILD_ROOT%{_unitdir}/unbound-anchor.service

%delete_la

install -d $RPM_BUILD_ROOT%{_rundir}/unbound

install -d $RPM_BUILD_ROOT%{_sysconfdir}/unbound/{keys.d,conf.d,local.d}
install -p %{SOURCE6} $RPM_BUILD_ROOT%{_sysconfdir}/unbound/keys.d/
install -p %{SOURCE7} $RPM_BUILD_ROOT%{_sysconfdir}/unbound/conf.d/
install -p %{SOURCE8} $RPM_BUILD_ROOT%{_sysconfdir}/unbound/local.d/

echo ".so man8/unbound-control.8" > $RPM_BUILD_ROOT/%{_mandir}/man8/unbound-control-setup.8

%pre libs
getent group unbound >/dev/null || groupadd -r unbound
getent passwd unbound >/dev/null || \
useradd -r -g unbound -d %{_sysconfdir}/unbound -s /sbin/nologin \
-c "Unbound DNS resolver" unbound

%post
%systemd_post unbound.service
%systemd_post unbound-keygen.service

%post anchor
%systemd_post unbound-anchor.service unbound-anchor.timer
# start the timer only if installing the package to prevent starting it, if it was stopped on purpose
if [ "$1" -eq 1 ]; then
    # the Unit is in presets, but would be started after reboot
    /bin/systemctl start unbound-anchor.timer >/dev/null 2>&1 || :
fi

%preun
%systemd_preun unbound.service
%systemd_preun unbound-keygen.service

%preun anchor
%systemd_preun unbound-anchor.service unbound-anchor.timer

%postun
%systemd_postun_with_restart unbound.service
%systemd_postun unbound-keygen.service

%postun anchor
%systemd_postun_with_restart unbound-anchor.service unbound-anchor.timer

%triggerun -- unbound < 1.4.12-4
/usr/bin/systemd-sysv-convert --save unbound >/dev/null 2>&1 ||:
/sbin/chkconfig --del unbound >/dev/null 2>&1 || :
/bin/systemctl try-restart unbound.service >/dev/null 2>&1 || :
/bin/systemctl try-restart unbound-keygen.service >/dev/null 2>&1 || :

%check
pushd %{name}-%{version}
make check
popd


%files
%defattr(-,root,root)
%doc doc/CREDITS doc/FEATURES doc/README doc/LICENSE
%attr(0644,root,root) %{_tmpfilesdir}/unbound.conf
%attr(0755,unbound,unbound) %dir %{_rundir}/%{name}
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/unbound.conf
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%dir %attr(0755,root,unbound) %{_sysconfdir}/%{name}/keys.d
%attr(0644,root,unbound) %config(noreplace) %{_sysconfdir}/%{name}/keys.d/*.key
%dir %attr(0755,root,unbound) %{_sysconfdir}/%{name}/conf.d
%attr(0644,root,unbound) %config(noreplace) %{_sysconfdir}/%{name}/conf.d/*.conf
%dir %attr(0755,root,unbound) %{_sysconfdir}/%{name}/local.d
%attr(0644,root,unbound) %config(noreplace) %{_sysconfdir}/%{name}/local.d/*.conf
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}-keygen.service
%ghost %attr(0640,root,unbound) %{_sysconfdir}/%{name}/unbound_control.pem
%ghost %attr(0640,root,unbound) %{_sysconfdir}/%{name}/unbound_control.key
%ghost %attr(0640,root,unbound) %{_sysconfdir}/%{name}/unbound_server.pem
%ghost %attr(0640,root,unbound) %{_sysconfdir}/%{name}/unbound_server.key
%{_sbindir}/unbound
%{_sbindir}/unbound-checkconf
%{_sbindir}/unbound-control
%{_sbindir}/unbound-control-setup

%files libs
%defattr(-,root,root)
%if %{?openEuler:1}0
%dir %attr(0755,unbound,unbound) %{_sharedstatedir}/%{name}
%attr(0644,unbound,unbound) %config %{_sharedstatedir}/%{name}/root.key
%else
%dir %attr(0755,unbound,unbound) %{_sharedstatedir}/%{name}
%attr(0600,unbound,unbound) %config %{_sharedstatedir}/%{name}/root.key
%endif
%{_libdir}/libunbound.so.*

%files -n python3-unbound
%defattr(-,root,root)
%doc pythonmod/examples/* libunbound/python/examples/* pythonmod/LICENSE
%{python3_sitearch}/*

%files devel
%defattr(-,root,root)
%{_libdir}/libunbound.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/*

%files anchor
%dir %attr(0755,root,root) %{_sysconfdir}/%{name}
%{_sbindir}/unbound-anchor
%attr(0644,root,root) %config %{_sysconfdir}/%{name}/root.key
%{_sysconfdir}/%{name}/icannbundle.pem
%{_unitdir}/unbound-anchor.timer
%{_unitdir}/unbound-anchor.service

%files utils
%{_sbindir}/unbound-host
%{_sbindir}/unbound-streamtcp

%files help
%defattr(-,root,root)
%{_mandir}/man*

%changelog
* Thu Jan 23 2025 gaihuiying <eaglegai@163.com> - 1.17.1-11
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:backport upstream to add check to fix coredump

* Wed Oct 16 2024 gaihuiying <eaglegai@163.com> - 1.17.1-10
- Type:cves
- CVE:CVE-2024-8508
- SUG:NA
- DESC:fix CVE-2024-8508

* Thu Aug 29 2024 gaihuiying <eaglegai@163.com> - 1.17.1-9
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:correct cve number

* Mon Aug 26 2024 gaihuiying <eaglegai@163.com> - 1.17.1-8
- Type:cves
- CVE:CVE-2024-43168
- SUG:NA
- DESC:fix CVE-2024-43168 better

* Mon Aug 19 2024 gaihuiying <eaglegai@163.com> - 1.17.1-7
- Type:cves
- CVE:CVE-2024-43167 CVE-2024-43168
- SUG:NA
- DESC:fix CVE-2024-43167 CVE-2024-43168

* Mon Jun 24 2024 gaihuiying <eaglegai@163.com> - 1.17.1-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:separate unbound-anchor and unbound-utils

* Fri May 17 2024 gaihuiying <eaglegai@163.com> - 1.17.1-5
- Type:cves
- CVE:CVE-2024-33655
- SUG:NA
- DESC:fix CVE-2024-33655

* Tue Mar 05 2024 gaihuiying <eaglegai@163.com> - 1.17.1-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:change ExecReload to fix reload failure

* Tue Feb 27 2024 gaihuiying <eaglegai@163.com> - 1.17.1-3
- Type:cves
- CVE:CVE-2024-1488
- SUG:NA
- DESC:fix CVE-2024-1488

* Fri Feb 23 2024 gaihuiying <eaglegai@163.com> - 1.17.1-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix CVE-2023-50387 CVE-2023-50868

* Tue Mar 07 2023 gaihuiying <eaglegai@163.com> - 1.17.1-1
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:update to 1.17.1

* Fri Mar 03 2023 gaihuiying <eaglegai@163.com> - 1.17.0-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:change run directory from /var/run to /run to fix "/usr/lib/tmpfiles.d/unbound.conf:1: xxxx"

* Tue Nov 08 2022 gaihuiying <eaglegai@163.com> - 1.17.0-1
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:update to 1.17.0

* Thu Sep 22 2022 xingwei <xingwei14@h-partners.com> - 1.13.2-6
- Type:cves
- CVE:CVE-2022-3204
- SUG:NA
- DESC:fix CVE-2022-3204

* Wed Aug 03 2022 yanglu <yanglu72@h-partners.com> - 1.13.2-5
- Type:cves
- CVE:CVE-2022-30698 CVE-2022-30699
- SUG:NA
- DESC:fix CVE-2022-30698 and CVE-2022-30699

* Tue Aug 02 2022 gaihuiying <eaglegai@163.com> - 1.13.2-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:rebuild to 1.13.2-4

* Sat Jun 11 2022 gaihuiying <eaglegai@163.com> - 1.13.2-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:remove buildin key
       add macros to control if key files permissons is 600 or 644 

* Mon Mar 21 2022 gaihuiying <eaglegai@163.com> - 1.13.2-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix -q doesn't work when use with 'unbound-control stats_shm'

* Sat Feb 19 2022 gaihuiying <eaglegai@163.com> - 1.13.2-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update unbound version to 1.13.2 and remove the dependency of python3.8

* Tue Aug 31 2021 gaihuiying <gaihuiying1@huawei.com> - 1.10.1-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix error when install unbound-libs

* Thu Aug 26 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.10.1-5
- Type: requirement
- ID:   NA
- SUG:  NA
- DESC: unbound-libs remove Requires systemd

* Thu May 27 2021 lijingyuan <lijingyuan3@huawei.com> - 1.10.1-4
- Type:bufix
- ID:NA
- SUG:NA
- DESC:Add the compilation dependency of gcc.

* Tue Feb 23 2021 zhouyihang <zhouyihang3@huawei.com> - 1.10.1-3
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2020-28935

* Thu Oct 29 2020 gaihuiying <gaihuiying1@huawei.com> - 1.10.1-2
- Type:requirement
- ID:NA
- SUG:NA
- DESC:remove python-universioned-command provided by python2

* Tue Jul 28 2020 gaihuiying <gaihuiying1@huawei.com> - 1.10.1-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update unbound version to 1.10.1

* Wed Feb 19 2020 hexiujun <hexiujun1@huawei.com> - 1.7.3-14
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:unpack libs subpackage 

* Sat Jan 11 2020 zhangrui<zhangrui182@huawei.com> - 1.7.3-13
- Type:cves
- ID:CVE-2019-18934
- SUG:restart
- DESC:fix CVE-2019-18934

* Tue Dec 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.7.3-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:change the jurisdiction for some path of files

* Wed Nov 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.7.3-11
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC:modify the contents of help package 

* Wed Sep 25 2019 Zaiwang Li <lizaiwang1@huawei.com> - 1.7.3-10
- Init Package
